package com.example.Vcriate.entity;

public enum NotificationStatus {
    PENDING, SENT, FAILED
}
