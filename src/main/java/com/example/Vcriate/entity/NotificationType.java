package com.example.Vcriate.entity;

public enum NotificationType {
    EMAIL, SMS, PUSH
}
