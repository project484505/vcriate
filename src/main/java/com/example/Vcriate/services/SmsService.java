package com.example.Vcriate.services;

import com.example.Vcriate.config.TwilioConfig;
import com.example.Vcriate.entity.Notification;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SmsService {

    private final TwilioConfig twilioConfig;

    @Autowired
    public SmsService(TwilioConfig twilioConfig) {
        this.twilioConfig = twilioConfig;
    }

    public void sendSms(Notification notification) {
        try {
            Message message = Message.creator(
                    new PhoneNumber(notification.getRecipient()), // To number
                    new PhoneNumber(twilioConfig.getPhoneNumber()), // From number
                    notification.getMessage()
            ).create();
            System.out.println("SMS sent successfully: " + message.getSid());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error sending SMS: " + e.getMessage());
        }
    }
}
