package com.example.Vcriate.services;
import com.example.Vcriate.entity.Notification;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import org.springframework.stereotype.Service;

@Service
public class PushNotificationService {

    public void sendPushNotification(Notification notification) {
        // Construct the message
        Message message = Message.builder()
                .putData("message", notification.getMessage())
                .setToken(notification.getRecipient()) // Assuming recipient is the device token
                .build();

        // Send the message
        try {
            String response = FirebaseMessaging.getInstance().send(message);
            System.out.println("Successfully sent push notification: " + response);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error sending push notification: " + e.getMessage());
        }
    }
}
