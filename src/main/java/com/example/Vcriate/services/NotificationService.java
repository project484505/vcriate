package com.example.Vcriate.services;

import com.example.Vcriate.entity.Notification;
import com.example.Vcriate.repo.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.example.Vcriate.entity.NotificationType.EMAIL;
import static com.example.Vcriate.entity.NotificationType.SMS;

@Service
public class NotificationService {
    @Autowired

    private EmailService emailService;
    @Autowired
    private SmsService smsService;
    @Autowired
    private PushNotificationService pushNotificationService;
    @Autowired
    private NotificationRepository notificationRepository;

    public void sendNotification(Notification notification) {
        switch (notification.getType()) {
            case EMAIL:
                emailService.sendEmail(notification);
                break;
            case SMS:
                smsService.sendSms(notification);
                break;
            case PUSH:
                pushNotificationService.sendPushNotification(notification);
                break;
        }
    }
}

